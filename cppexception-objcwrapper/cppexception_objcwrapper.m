#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "forcppexception.h"

void foo() {
    throw std::exception();
}

void bar() {
}

@interface testTests : XCTestCase
@end

@implementation testTests
- (void)testExample
{
    XCTAssertCppThrows(foo(), @"should throw exception");    // succeed
    XCTAssertCppThrows(bar(), @"should throw exception");    // failed
}
@end
